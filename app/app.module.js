(function () {
    angular.module("TestApp", [
        "TestApp.core",
        //pages
        "TestApp.test",
    ]);
})();
