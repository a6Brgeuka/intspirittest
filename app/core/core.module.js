(function () {
    angular.module("TestApp.core", [
      'TestApp.blocks',

      //libs
      'ui.router',
      'ui.bootstrap'
    ]);
})();
