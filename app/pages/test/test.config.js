(function () {

    angular
      .module("TestApp.test")
      .config(config);

    config.$inject = ["$stateProvider"];

    function config($stateProvider) {
      $stateProvider
        .state("home", {
          url: "/",
          templateUrl: "app/pages/test/test.html",
          controller:'TestController as test'
        })
        .state("home.first", {
          url: "first",
          templateUrl: "app/pages/test/first/first.html",
          controller:'FirstController as first'
        })
        .state("home.second", {
          url: "second",
          templateUrl: "app/pages/test/second/second.html",
          controller:'SecondController as second'
        })
        .state("home.third", {
          url: "third",
          templateUrl: "app/pages/test/third/third.html",
          controller:'ThirdController as third'
        });
    };
})();
