(function () {
  angular
    .module("TestApp.test")
    .controller("TestController", testController);

  testController.$inject = [];

  function testController() {
    var vm = this;

    vm.challenges = [
      {name: "First challenge", state: "first"},
      {name: "Second challenge", state: "second"},
      {name: "Third challenge", state: "third"}
    ];
  };
})();
