(function () {
  angular
    .module("TestApp.test")
    .controller("FirstController", firstController);

  firstController.$inject = ["TestService"];

  function firstController(TestService) {
    var vm = this;
    vm.data = {
      request : ""
    };

    vm.buttonName = "Submit"

    vm.alerts = [];

    vm.send = send;
    vm.closeAlert = closeAlert;

    function send() {
      var empty = isEmpty(vm.data.request);

      if(empty){
        if(vm.alerts.length == 1 && vm.alerts[0].msg == "No Content"){
          vm.alerts = [];
        }
        if(vm.alerts.length == 5) {
          vm.alerts.splice(vm.alerts.length - 1, 1);
        }
        vm.alerts.push({type: 'danger', msg: 'Input is empty'});
        vm.buttonName = "Resubmit";
      } else {
        TestService
          .firstChallenge(vm.data)
          .then(success)
          .catch(error);

        function success(response) {
          vm.data.request = "";
          vm.buttonName = "Submit";
          vm.alerts = [];
          if(response.status == 200){
            vm.alerts.push({type: 'success', msg: response.data});
          } else {
            vm.alerts.push({type: 'danger', msg: response.statusText});
          }
        }

        function error(error) {
          vm.data.request = "";
          vm.buttonName = "Submit"
          vm.alerts = [];
          vm.alerts.push({type: 'danger', msg: "servers error"});
        }
      }
    }

    function closeAlert(index) {
      vm.alerts.splice(index, 1);
    }

    function isEmpty(request) {
      return request == "" ? true : false;
    }
  };
})();
