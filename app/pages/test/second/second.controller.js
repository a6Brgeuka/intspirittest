(function () {
  angular
    .module("TestApp.test")
    .controller("SecondController", secondController);

  secondController.$inject = ["TestService"];

  function secondController(TestService) {
    var vm = this;
    vm.color = "white";
    vm.error = {
      /**/
    };
    vm.countErrors = 0;
    vm.countSuccess = 0;
    vm.countClicks = 0;
    vm.percentage = 0;
    vm.strickErrors = 0;
    vm.countForPercentage = 0;
    vm.send = send;

    function send() {
      TestService
        .secondChallenge()
        .then(function (response) {
          if(response.data.result){
            vm.color = "green";
            vm.strickErrors = 0;
            vm.countSuccess++;
            vm.countClicks++;
            vm.percentage = ((vm.countForPercentage / vm.countClicks) * 100).toFixed(2);
          } else {
            vm.color = "red";
            vm.countForPercentage++;
            vm.countErrors++;
            vm.strickErrors++;
            vm.countClicks++;
            vm.percentage = ((vm.countForPercentage / vm.countClicks) * 100).toFixed(2);
          }
        })
        .catch(function (error) {
          console.log(error);
        })
        .finally(function () {
          if(vm.countClicks == 3){
            //vm.percentage = 0;
            vm.countForPercentage = 0;
            vm.countClicks = 0;
          }
        });
    }
  };
})();
