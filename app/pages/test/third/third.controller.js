(function () {
  angular
    .module("TestApp.test")
    .controller("ThirdController", thirdController);

  thirdController.$inject = ["TestService"];

  function thirdController(TestService) {
    var vm = this;

    vm.fruits = [];
    vm.vegetables = [];
    vm.getData = getData;
    vm.clearFetch = clearFetch;
    vm.fruitObj = {};
    vm.vegetablesObj = {};

    function getData() {
      TestService
        .thirdChallenge()
        .then(function (response) {
          if(response.data.type == "vegetable"){
            vm.vegetables.push(response.data.item);
            fetchData(vm.vegetables, response.data.type);
          } else {
            vm.fruits.push(response.data.item);
            fetchData(vm.fruits);
          }

          //console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    function fetchData(array, type){
      var initialValue = {};

      if(type == "vegetable"){
        vm.vegetablesObj = array.reduce(reducer, initialValue);
      } else {
        vm.fruitObj = array.reduce(reducer, initialValue);
      }

      function reducer(fruit, count){
          if(!fruit[count]){
              fruit[count] = 1;
          } else {
              fruit[count] = fruit[count] + 1;
          }
          return fruit;
      };
    }

    function clearFetch() {
      vm.vegetablesObj = vm.fruitObj = {};
      vm.vegetables = vm.fruits = [];
    }
  };
})();
