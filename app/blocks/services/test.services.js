(function(){
  'use strict';

  angular
    .module('blocks.services')
    .factory('TestService', testService);

      testService.$inject = ['$http', 'BASE_INFO'];

      function testService($http, BASE_INFO) {
        var services = {
          firstChallenge : firstChallenge,
          secondChallenge : secondChallenge,
          thirdChallenge : thirdChallenge
        }

        var apiURL = BASE_INFO.URL;

        return services;

        function firstChallenge(data) {
          return $http.post(apiURL + "/post_response", data);
        }

        function secondChallenge() {
          return $http.get(apiURL + "/response_codes");
        }

        function thirdChallenge() {
          return $http.get(apiURL + "/data_set");
        }
      }
})();
