(function () {
    angular.module("blocks.constantsService")
        .constant('BASE_INFO', {
            URL: 'http://careers.intspirit.com/endpoint',
            PORT: '',
            API_URL: ''
        })
        .constant('ERROR_CODES', {
            notFound: "not_found",
            unknown: "unknown_error"
        });
})();
