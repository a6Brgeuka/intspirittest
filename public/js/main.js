(function () {
    angular.module("TestApp", [
        "TestApp.core",
        //pages
        "TestApp.test",
    ]);
})();

(function () {
    angular.module("TestApp.core", [
      'TestApp.blocks',

      //libs
      'ui.router',
      'ui.bootstrap'
    ]);
})();

(function () {
    angular.module("TestApp.blocks", [
        "blocks.services",
        "blocks.constantsService"
    ]);
})();

(function () {
    angular.module("blocks.constantsService", []);
})();

(function () {
    angular.module("blocks.services", [

    ]);
})();

(function () {
    angular.module("TestApp.test", [
        "TestApp.core"
    ]);
})();

(function () {

  angular
    .module("TestApp")
    .config(config);

  config.$inject = ["$stateProvider", "$urlRouterProvider", "$locationProvider"];

  function config($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise("/");
  };
})();

(function() {
    'use strict';

    angular
        .module("TestApp.core")
        .controller('CoreController', coreController);

    coreController.$inject = [];

    function coreController(){
        var vm = this;
    }
})();

(function () {
    angular.module("blocks.constantsService")
        .constant('BASE_INFO', {
            URL: 'http://careers.intspirit.com/endpoint',
            PORT: '',
            API_URL: ''
        })
        .constant('ERROR_CODES', {
            notFound: "not_found",
            unknown: "unknown_error"
        });
})();

(function(){
  'use strict';

  angular
    .module('blocks.services')
    .factory('TestService', testService);

      testService.$inject = ['$http', 'BASE_INFO'];

      function testService($http, BASE_INFO) {
        var services = {
          firstChallenge : firstChallenge,
          secondChallenge : secondChallenge,
          thirdChallenge : thirdChallenge
        }

        var apiURL = BASE_INFO.URL;

        return services;

        function firstChallenge(data) {
          return $http.post(apiURL + "/post_response", data);
        }

        function secondChallenge() {
          return $http.get(apiURL + "/response_codes");
        }

        function thirdChallenge() {
          return $http.get(apiURL + "/data_set");
        }
      }
})();

(function () {

    angular
      .module("TestApp.test")
      .config(config);

    config.$inject = ["$stateProvider"];

    function config($stateProvider) {
      $stateProvider
        .state("home", {
          url: "/",
          templateUrl: "app/pages/test/test.html",
          controller:'TestController as test'
        })
        .state("home.first", {
          url: "first",
          templateUrl: "app/pages/test/first/first.html",
          controller:'FirstController as first'
        })
        .state("home.second", {
          url: "second",
          templateUrl: "app/pages/test/second/second.html",
          controller:'SecondController as second'
        })
        .state("home.third", {
          url: "third",
          templateUrl: "app/pages/test/third/third.html",
          controller:'ThirdController as third'
        });
    };
})();

(function () {
  angular
    .module("TestApp.test")
    .controller("TestController", testController);

  testController.$inject = [];

  function testController() {
    var vm = this;

    vm.challenges = [
      {name: "First challenge", state: "first"},
      {name: "Second challenge", state: "second"},
      {name: "Third challenge", state: "third"}
    ];
  };
})();

(function () {
  angular
    .module("TestApp.test")
    .controller("FirstController", firstController);

  firstController.$inject = ["TestService"];

  function firstController(TestService) {
    var vm = this;
    vm.data = {
      request : ""
    };

    vm.buttonName = "Submit"

    vm.alerts = [];

    vm.send = send;
    vm.closeAlert = closeAlert;

    function send() {
      var empty = isEmpty(vm.data.request);

      if(empty){
        if(vm.alerts.length == 1 && vm.alerts[0].msg == "No Content"){
          vm.alerts = [];
        }
        if(vm.alerts.length == 5) {
          vm.alerts.splice(vm.alerts.length - 1, 1);
        }
        vm.alerts.push({type: 'danger', msg: 'Input is empty'});
        vm.buttonName = "Resubmit";
      } else {
        TestService
          .firstChallenge(vm.data)
          .then(success)
          .catch(error);

        function success(response) {
          vm.data.request = "";
          vm.buttonName = "Submit";
          vm.alerts = [];
          if(response.status == 200){
            vm.alerts.push({type: 'success', msg: response.data});
          } else {
            vm.alerts.push({type: 'danger', msg: response.statusText});
          }
        }

        function error(error) {
          vm.data.request = "";
          vm.buttonName = "Submit"
          vm.alerts = [];
          vm.alerts.push({type: 'danger', msg: "servers error"});
        }
      }
    }

    function closeAlert(index) {
      vm.alerts.splice(index, 1);
    }

    function isEmpty(request) {
      return request == "" ? true : false;
    }
  };
})();

(function () {
  angular
    .module("TestApp.test")
    .controller("SecondController", secondController);

  secondController.$inject = ["TestService"];

  function secondController(TestService) {
    var vm = this;
    vm.color = "white";
    vm.error = {
      /**/
    };
    vm.countErrors = 0;
    vm.countSuccess = 0;
    vm.countClicks = 0;
    vm.percentage = 0;
    vm.strickErrors = 0;
    vm.countForPercentage = 0;
    vm.send = send;

    function send() {
      TestService
        .secondChallenge()
        .then(function (response) {
          if(response.data.result){
            vm.color = "green";
            vm.strickErrors = 0;
            vm.countSuccess++;
            vm.countClicks++;
            vm.percentage = ((vm.countForPercentage / vm.countClicks) * 100).toFixed(2);
          } else {
            vm.color = "red";
            vm.countForPercentage++;
            vm.countErrors++;
            vm.strickErrors++;
            vm.countClicks++;
            vm.percentage = ((vm.countForPercentage / vm.countClicks) * 100).toFixed(2);
          }
        })
        .catch(function (error) {
          console.log(error);
        })
        .finally(function () {
          if(vm.countClicks == 3){
            //vm.percentage = 0;
            vm.countForPercentage = 0;
            vm.countClicks = 0;
          }
        });
    }
  };
})();

(function () {
  angular
    .module("TestApp.test")
    .controller("ThirdController", thirdController);

  thirdController.$inject = ["TestService"];

  function thirdController(TestService) {
    var vm = this;

    vm.fruits = [];
    vm.vegetables = [];
    vm.getData = getData;
    vm.clearFetch = clearFetch;
    vm.fruitObj = {};
    vm.vegetablesObj = {};

    function getData() {
      TestService
        .thirdChallenge()
        .then(function (response) {
          if(response.data.type == "vegetable"){
            vm.vegetables.push(response.data.item);
            fetchData(vm.vegetables, response.data.type);
          } else {
            vm.fruits.push(response.data.item);
            fetchData(vm.fruits);
          }

          //console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    function fetchData(array, type){
      var initialValue = {};

      if(type == "vegetable"){
        vm.vegetablesObj = array.reduce(reducer, initialValue);
      } else {
        vm.fruitObj = array.reduce(reducer, initialValue);
      }

      function reducer(fruit, count){
          if(!fruit[count]){
              fruit[count] = 1;
          } else {
              fruit[count] = fruit[count] + 1;
          }
          return fruit;
      };
    }

    function clearFetch() {
      vm.vegetablesObj = vm.fruitObj = {};
      vm.vegetables = vm.fruits = [];
    }
  };
})();

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUuanMiLCJjb3JlL2NvcmUubW9kdWxlLmpzIiwiYmxvY2tzL2Jsb2Nrcy5tb2R1bGUuanMiLCJibG9ja3MvY29uc3RhbnRzL2NvbnN0YW5zLm1vZHVsZS5qcyIsImJsb2Nrcy9zZXJ2aWNlcy9zZXJ2aWNlLm1vZHVsZS5qcyIsInBhZ2VzL3Rlc3QvdGVzdC5tb2R1bGUuanMiLCJhcHAuY29uZmlnLmpzIiwiY29yZS9jb3JlLmNvbnRyb2xsZXIuanMiLCJibG9ja3MvY29uc3RhbnRzL2NvbnN0YW5zLnNlcnZpY2UuanMiLCJibG9ja3Mvc2VydmljZXMvdGVzdC5zZXJ2aWNlcy5qcyIsInBhZ2VzL3Rlc3QvdGVzdC5jb25maWcuanMiLCJwYWdlcy90ZXN0L3Rlc3QuY29udHJvbGxlci5qcyIsInBhZ2VzL3Rlc3QvZmlyc3QvZmlyc3QuY29udHJvbGxlci5qcyIsInBhZ2VzL3Rlc3Qvc2Vjb25kL3NlY29uZC5jb250cm9sbGVyLmpzIiwicGFnZXMvdGVzdC90aGlyZC90aGlyZC5jb250cm9sbGVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUNIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDYkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2pCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbkVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3JEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKCkge1xyXG4gICAgYW5ndWxhci5tb2R1bGUoXCJUZXN0QXBwXCIsIFtcclxuICAgICAgICBcIlRlc3RBcHAuY29yZVwiLFxyXG4gICAgICAgIC8vcGFnZXNcclxuICAgICAgICBcIlRlc3RBcHAudGVzdFwiLFxyXG4gICAgXSk7XHJcbn0pKCk7XHJcbiIsIihmdW5jdGlvbiAoKSB7XHJcbiAgICBhbmd1bGFyLm1vZHVsZShcIlRlc3RBcHAuY29yZVwiLCBbXHJcbiAgICAgICdUZXN0QXBwLmJsb2NrcycsXHJcblxyXG4gICAgICAvL2xpYnNcclxuICAgICAgJ3VpLnJvdXRlcicsXHJcbiAgICAgICd1aS5ib290c3RyYXAnXHJcbiAgICBdKTtcclxufSkoKTtcclxuIiwiKGZ1bmN0aW9uICgpIHtcclxuICAgIGFuZ3VsYXIubW9kdWxlKFwiVGVzdEFwcC5ibG9ja3NcIiwgW1xyXG4gICAgICAgIFwiYmxvY2tzLnNlcnZpY2VzXCIsXHJcbiAgICAgICAgXCJibG9ja3MuY29uc3RhbnRzU2VydmljZVwiXHJcbiAgICBdKTtcclxufSkoKTtcclxuIiwiKGZ1bmN0aW9uICgpIHtcclxuICAgIGFuZ3VsYXIubW9kdWxlKFwiYmxvY2tzLmNvbnN0YW50c1NlcnZpY2VcIiwgW10pO1xyXG59KSgpO1xyXG4iLCIoZnVuY3Rpb24gKCkge1xyXG4gICAgYW5ndWxhci5tb2R1bGUoXCJibG9ja3Muc2VydmljZXNcIiwgW1xyXG5cclxuICAgIF0pO1xyXG59KSgpO1xyXG4iLCIoZnVuY3Rpb24gKCkge1xyXG4gICAgYW5ndWxhci5tb2R1bGUoXCJUZXN0QXBwLnRlc3RcIiwgW1xyXG4gICAgICAgIFwiVGVzdEFwcC5jb3JlXCJcclxuICAgIF0pO1xyXG59KSgpO1xyXG4iLCIoZnVuY3Rpb24gKCkge1xyXG5cclxuICBhbmd1bGFyXHJcbiAgICAubW9kdWxlKFwiVGVzdEFwcFwiKVxyXG4gICAgLmNvbmZpZyhjb25maWcpO1xyXG5cclxuICBjb25maWcuJGluamVjdCA9IFtcIiRzdGF0ZVByb3ZpZGVyXCIsIFwiJHVybFJvdXRlclByb3ZpZGVyXCIsIFwiJGxvY2F0aW9uUHJvdmlkZXJcIl07XHJcblxyXG4gIGZ1bmN0aW9uIGNvbmZpZygkc3RhdGVQcm92aWRlciwgJHVybFJvdXRlclByb3ZpZGVyLCAkbG9jYXRpb25Qcm92aWRlcikge1xyXG5cclxuICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoXCIvXCIpO1xyXG4gIH07XHJcbn0pKCk7XHJcbiIsIihmdW5jdGlvbigpIHtcclxuICAgICd1c2Ugc3RyaWN0JztcclxuXHJcbiAgICBhbmd1bGFyXHJcbiAgICAgICAgLm1vZHVsZShcIlRlc3RBcHAuY29yZVwiKVxyXG4gICAgICAgIC5jb250cm9sbGVyKCdDb3JlQ29udHJvbGxlcicsIGNvcmVDb250cm9sbGVyKTtcclxuXHJcbiAgICBjb3JlQ29udHJvbGxlci4kaW5qZWN0ID0gW107XHJcblxyXG4gICAgZnVuY3Rpb24gY29yZUNvbnRyb2xsZXIoKXtcclxuICAgICAgICB2YXIgdm0gPSB0aGlzO1xyXG4gICAgfVxyXG59KSgpO1xyXG4iLCIoZnVuY3Rpb24gKCkge1xyXG4gICAgYW5ndWxhci5tb2R1bGUoXCJibG9ja3MuY29uc3RhbnRzU2VydmljZVwiKVxyXG4gICAgICAgIC5jb25zdGFudCgnQkFTRV9JTkZPJywge1xyXG4gICAgICAgICAgICBVUkw6ICdodHRwOi8vY2FyZWVycy5pbnRzcGlyaXQuY29tL2VuZHBvaW50JyxcclxuICAgICAgICAgICAgUE9SVDogJycsXHJcbiAgICAgICAgICAgIEFQSV9VUkw6ICcnXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY29uc3RhbnQoJ0VSUk9SX0NPREVTJywge1xyXG4gICAgICAgICAgICBub3RGb3VuZDogXCJub3RfZm91bmRcIixcclxuICAgICAgICAgICAgdW5rbm93bjogXCJ1bmtub3duX2Vycm9yXCJcclxuICAgICAgICB9KTtcclxufSkoKTtcclxuIiwiKGZ1bmN0aW9uKCl7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBhbmd1bGFyXHJcbiAgICAubW9kdWxlKCdibG9ja3Muc2VydmljZXMnKVxyXG4gICAgLmZhY3RvcnkoJ1Rlc3RTZXJ2aWNlJywgdGVzdFNlcnZpY2UpO1xyXG5cclxuICAgICAgdGVzdFNlcnZpY2UuJGluamVjdCA9IFsnJGh0dHAnLCAnQkFTRV9JTkZPJ107XHJcblxyXG4gICAgICBmdW5jdGlvbiB0ZXN0U2VydmljZSgkaHR0cCwgQkFTRV9JTkZPKSB7XHJcbiAgICAgICAgdmFyIHNlcnZpY2VzID0ge1xyXG4gICAgICAgICAgZmlyc3RDaGFsbGVuZ2UgOiBmaXJzdENoYWxsZW5nZSxcclxuICAgICAgICAgIHNlY29uZENoYWxsZW5nZSA6IHNlY29uZENoYWxsZW5nZSxcclxuICAgICAgICAgIHRoaXJkQ2hhbGxlbmdlIDogdGhpcmRDaGFsbGVuZ2VcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciBhcGlVUkwgPSBCQVNFX0lORk8uVVJMO1xyXG5cclxuICAgICAgICByZXR1cm4gc2VydmljZXM7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGZpcnN0Q2hhbGxlbmdlKGRhdGEpIHtcclxuICAgICAgICAgIHJldHVybiAkaHR0cC5wb3N0KGFwaVVSTCArIFwiL3Bvc3RfcmVzcG9uc2VcIiwgZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBmdW5jdGlvbiBzZWNvbmRDaGFsbGVuZ2UoKSB7XHJcbiAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KGFwaVVSTCArIFwiL3Jlc3BvbnNlX2NvZGVzXCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZnVuY3Rpb24gdGhpcmRDaGFsbGVuZ2UoKSB7XHJcbiAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KGFwaVVSTCArIFwiL2RhdGFfc2V0XCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG59KSgpO1xyXG4iLCIoZnVuY3Rpb24gKCkge1xyXG5cclxuICAgIGFuZ3VsYXJcclxuICAgICAgLm1vZHVsZShcIlRlc3RBcHAudGVzdFwiKVxyXG4gICAgICAuY29uZmlnKGNvbmZpZyk7XHJcblxyXG4gICAgY29uZmlnLiRpbmplY3QgPSBbXCIkc3RhdGVQcm92aWRlclwiXTtcclxuXHJcbiAgICBmdW5jdGlvbiBjb25maWcoJHN0YXRlUHJvdmlkZXIpIHtcclxuICAgICAgJHN0YXRlUHJvdmlkZXJcclxuICAgICAgICAuc3RhdGUoXCJob21lXCIsIHtcclxuICAgICAgICAgIHVybDogXCIvXCIsXHJcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogXCJhcHAvcGFnZXMvdGVzdC90ZXN0Lmh0bWxcIixcclxuICAgICAgICAgIGNvbnRyb2xsZXI6J1Rlc3RDb250cm9sbGVyIGFzIHRlc3QnXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuc3RhdGUoXCJob21lLmZpcnN0XCIsIHtcclxuICAgICAgICAgIHVybDogXCJmaXJzdFwiLFxyXG4gICAgICAgICAgdGVtcGxhdGVVcmw6IFwiYXBwL3BhZ2VzL3Rlc3QvZmlyc3QvZmlyc3QuaHRtbFwiLFxyXG4gICAgICAgICAgY29udHJvbGxlcjonRmlyc3RDb250cm9sbGVyIGFzIGZpcnN0J1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnN0YXRlKFwiaG9tZS5zZWNvbmRcIiwge1xyXG4gICAgICAgICAgdXJsOiBcInNlY29uZFwiLFxyXG4gICAgICAgICAgdGVtcGxhdGVVcmw6IFwiYXBwL3BhZ2VzL3Rlc3Qvc2Vjb25kL3NlY29uZC5odG1sXCIsXHJcbiAgICAgICAgICBjb250cm9sbGVyOidTZWNvbmRDb250cm9sbGVyIGFzIHNlY29uZCdcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5zdGF0ZShcImhvbWUudGhpcmRcIiwge1xyXG4gICAgICAgICAgdXJsOiBcInRoaXJkXCIsXHJcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogXCJhcHAvcGFnZXMvdGVzdC90aGlyZC90aGlyZC5odG1sXCIsXHJcbiAgICAgICAgICBjb250cm9sbGVyOidUaGlyZENvbnRyb2xsZXIgYXMgdGhpcmQnXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59KSgpO1xyXG4iLCIoZnVuY3Rpb24gKCkge1xyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoXCJUZXN0QXBwLnRlc3RcIilcclxuICAgIC5jb250cm9sbGVyKFwiVGVzdENvbnRyb2xsZXJcIiwgdGVzdENvbnRyb2xsZXIpO1xyXG5cclxuICB0ZXN0Q29udHJvbGxlci4kaW5qZWN0ID0gW107XHJcblxyXG4gIGZ1bmN0aW9uIHRlc3RDb250cm9sbGVyKCkge1xyXG4gICAgdmFyIHZtID0gdGhpcztcclxuXHJcbiAgICB2bS5jaGFsbGVuZ2VzID0gW1xyXG4gICAgICB7bmFtZTogXCJGaXJzdCBjaGFsbGVuZ2VcIiwgc3RhdGU6IFwiZmlyc3RcIn0sXHJcbiAgICAgIHtuYW1lOiBcIlNlY29uZCBjaGFsbGVuZ2VcIiwgc3RhdGU6IFwic2Vjb25kXCJ9LFxyXG4gICAgICB7bmFtZTogXCJUaGlyZCBjaGFsbGVuZ2VcIiwgc3RhdGU6IFwidGhpcmRcIn1cclxuICAgIF07XHJcbiAgfTtcclxufSkoKTtcclxuIiwiKGZ1bmN0aW9uICgpIHtcclxuICBhbmd1bGFyXHJcbiAgICAubW9kdWxlKFwiVGVzdEFwcC50ZXN0XCIpXHJcbiAgICAuY29udHJvbGxlcihcIkZpcnN0Q29udHJvbGxlclwiLCBmaXJzdENvbnRyb2xsZXIpO1xyXG5cclxuICBmaXJzdENvbnRyb2xsZXIuJGluamVjdCA9IFtcIlRlc3RTZXJ2aWNlXCJdO1xyXG5cclxuICBmdW5jdGlvbiBmaXJzdENvbnRyb2xsZXIoVGVzdFNlcnZpY2UpIHtcclxuICAgIHZhciB2bSA9IHRoaXM7XHJcbiAgICB2bS5kYXRhID0ge1xyXG4gICAgICByZXF1ZXN0IDogXCJcIlxyXG4gICAgfTtcclxuXHJcbiAgICB2bS5idXR0b25OYW1lID0gXCJTdWJtaXRcIlxyXG5cclxuICAgIHZtLmFsZXJ0cyA9IFtdO1xyXG5cclxuICAgIHZtLnNlbmQgPSBzZW5kO1xyXG4gICAgdm0uY2xvc2VBbGVydCA9IGNsb3NlQWxlcnQ7XHJcblxyXG4gICAgZnVuY3Rpb24gc2VuZCgpIHtcclxuICAgICAgdmFyIGVtcHR5ID0gaXNFbXB0eSh2bS5kYXRhLnJlcXVlc3QpO1xyXG5cclxuICAgICAgaWYoZW1wdHkpe1xyXG4gICAgICAgIGlmKHZtLmFsZXJ0cy5sZW5ndGggPT0gMSAmJiB2bS5hbGVydHNbMF0ubXNnID09IFwiTm8gQ29udGVudFwiKXtcclxuICAgICAgICAgIHZtLmFsZXJ0cyA9IFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZih2bS5hbGVydHMubGVuZ3RoID09IDUpIHtcclxuICAgICAgICAgIHZtLmFsZXJ0cy5zcGxpY2Uodm0uYWxlcnRzLmxlbmd0aCAtIDEsIDEpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB2bS5hbGVydHMucHVzaCh7dHlwZTogJ2RhbmdlcicsIG1zZzogJ0lucHV0IGlzIGVtcHR5J30pO1xyXG4gICAgICAgIHZtLmJ1dHRvbk5hbWUgPSBcIlJlc3VibWl0XCI7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgVGVzdFNlcnZpY2VcclxuICAgICAgICAgIC5maXJzdENoYWxsZW5nZSh2bS5kYXRhKVxyXG4gICAgICAgICAgLnRoZW4oc3VjY2VzcylcclxuICAgICAgICAgIC5jYXRjaChlcnJvcik7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIHN1Y2Nlc3MocmVzcG9uc2UpIHtcclxuICAgICAgICAgIHZtLmRhdGEucmVxdWVzdCA9IFwiXCI7XHJcbiAgICAgICAgICB2bS5idXR0b25OYW1lID0gXCJTdWJtaXRcIjtcclxuICAgICAgICAgIHZtLmFsZXJ0cyA9IFtdO1xyXG4gICAgICAgICAgaWYocmVzcG9uc2Uuc3RhdHVzID09IDIwMCl7XHJcbiAgICAgICAgICAgIHZtLmFsZXJ0cy5wdXNoKHt0eXBlOiAnc3VjY2VzcycsIG1zZzogcmVzcG9uc2UuZGF0YX0pO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdm0uYWxlcnRzLnB1c2goe3R5cGU6ICdkYW5nZXInLCBtc2c6IHJlc3BvbnNlLnN0YXR1c1RleHR9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGVycm9yKGVycm9yKSB7XHJcbiAgICAgICAgICB2bS5kYXRhLnJlcXVlc3QgPSBcIlwiO1xyXG4gICAgICAgICAgdm0uYnV0dG9uTmFtZSA9IFwiU3VibWl0XCJcclxuICAgICAgICAgIHZtLmFsZXJ0cyA9IFtdO1xyXG4gICAgICAgICAgdm0uYWxlcnRzLnB1c2goe3R5cGU6ICdkYW5nZXInLCBtc2c6IFwic2VydmVycyBlcnJvclwifSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gY2xvc2VBbGVydChpbmRleCkge1xyXG4gICAgICB2bS5hbGVydHMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBpc0VtcHR5KHJlcXVlc3QpIHtcclxuICAgICAgcmV0dXJuIHJlcXVlc3QgPT0gXCJcIiA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuICB9O1xyXG59KSgpO1xyXG4iLCIoZnVuY3Rpb24gKCkge1xyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoXCJUZXN0QXBwLnRlc3RcIilcclxuICAgIC5jb250cm9sbGVyKFwiU2Vjb25kQ29udHJvbGxlclwiLCBzZWNvbmRDb250cm9sbGVyKTtcclxuXHJcbiAgc2Vjb25kQ29udHJvbGxlci4kaW5qZWN0ID0gW1wiVGVzdFNlcnZpY2VcIl07XHJcblxyXG4gIGZ1bmN0aW9uIHNlY29uZENvbnRyb2xsZXIoVGVzdFNlcnZpY2UpIHtcclxuICAgIHZhciB2bSA9IHRoaXM7XHJcbiAgICB2bS5jb2xvciA9IFwid2hpdGVcIjtcclxuICAgIHZtLmVycm9yID0ge1xyXG4gICAgICAvKiovXHJcbiAgICB9O1xyXG4gICAgdm0uY291bnRFcnJvcnMgPSAwO1xyXG4gICAgdm0uY291bnRTdWNjZXNzID0gMDtcclxuICAgIHZtLmNvdW50Q2xpY2tzID0gMDtcclxuICAgIHZtLnBlcmNlbnRhZ2UgPSAwO1xyXG4gICAgdm0uc3RyaWNrRXJyb3JzID0gMDtcclxuICAgIHZtLmNvdW50Rm9yUGVyY2VudGFnZSA9IDA7XHJcbiAgICB2bS5zZW5kID0gc2VuZDtcclxuXHJcbiAgICBmdW5jdGlvbiBzZW5kKCkge1xyXG4gICAgICBUZXN0U2VydmljZVxyXG4gICAgICAgIC5zZWNvbmRDaGFsbGVuZ2UoKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgaWYocmVzcG9uc2UuZGF0YS5yZXN1bHQpe1xyXG4gICAgICAgICAgICB2bS5jb2xvciA9IFwiZ3JlZW5cIjtcclxuICAgICAgICAgICAgdm0uc3RyaWNrRXJyb3JzID0gMDtcclxuICAgICAgICAgICAgdm0uY291bnRTdWNjZXNzKys7XHJcbiAgICAgICAgICAgIHZtLmNvdW50Q2xpY2tzKys7XHJcbiAgICAgICAgICAgIHZtLnBlcmNlbnRhZ2UgPSAoKHZtLmNvdW50Rm9yUGVyY2VudGFnZSAvIHZtLmNvdW50Q2xpY2tzKSAqIDEwMCkudG9GaXhlZCgyKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZtLmNvbG9yID0gXCJyZWRcIjtcclxuICAgICAgICAgICAgdm0uY291bnRGb3JQZXJjZW50YWdlKys7XHJcbiAgICAgICAgICAgIHZtLmNvdW50RXJyb3JzKys7XHJcbiAgICAgICAgICAgIHZtLnN0cmlja0Vycm9ycysrO1xyXG4gICAgICAgICAgICB2bS5jb3VudENsaWNrcysrO1xyXG4gICAgICAgICAgICB2bS5wZXJjZW50YWdlID0gKCh2bS5jb3VudEZvclBlcmNlbnRhZ2UgLyB2bS5jb3VudENsaWNrcykgKiAxMDApLnRvRml4ZWQoMik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGVycm9yKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAuZmluYWxseShmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICBpZih2bS5jb3VudENsaWNrcyA9PSAzKXtcclxuICAgICAgICAgICAgLy92bS5wZXJjZW50YWdlID0gMDtcclxuICAgICAgICAgICAgdm0uY291bnRGb3JQZXJjZW50YWdlID0gMDtcclxuICAgICAgICAgICAgdm0uY291bnRDbGlja3MgPSAwO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH07XHJcbn0pKCk7XHJcbiIsIihmdW5jdGlvbiAoKSB7XHJcbiAgYW5ndWxhclxyXG4gICAgLm1vZHVsZShcIlRlc3RBcHAudGVzdFwiKVxyXG4gICAgLmNvbnRyb2xsZXIoXCJUaGlyZENvbnRyb2xsZXJcIiwgdGhpcmRDb250cm9sbGVyKTtcclxuXHJcbiAgdGhpcmRDb250cm9sbGVyLiRpbmplY3QgPSBbXCJUZXN0U2VydmljZVwiXTtcclxuXHJcbiAgZnVuY3Rpb24gdGhpcmRDb250cm9sbGVyKFRlc3RTZXJ2aWNlKSB7XHJcbiAgICB2YXIgdm0gPSB0aGlzO1xyXG5cclxuICAgIHZtLmZydWl0cyA9IFtdO1xyXG4gICAgdm0udmVnZXRhYmxlcyA9IFtdO1xyXG4gICAgdm0uZ2V0RGF0YSA9IGdldERhdGE7XHJcbiAgICB2bS5jbGVhckZldGNoID0gY2xlYXJGZXRjaDtcclxuICAgIHZtLmZydWl0T2JqID0ge307XHJcbiAgICB2bS52ZWdldGFibGVzT2JqID0ge307XHJcblxyXG4gICAgZnVuY3Rpb24gZ2V0RGF0YSgpIHtcclxuICAgICAgVGVzdFNlcnZpY2VcclxuICAgICAgICAudGhpcmRDaGFsbGVuZ2UoKVxyXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgaWYocmVzcG9uc2UuZGF0YS50eXBlID09IFwidmVnZXRhYmxlXCIpe1xyXG4gICAgICAgICAgICB2bS52ZWdldGFibGVzLnB1c2gocmVzcG9uc2UuZGF0YS5pdGVtKTtcclxuICAgICAgICAgICAgZmV0Y2hEYXRhKHZtLnZlZ2V0YWJsZXMsIHJlc3BvbnNlLmRhdGEudHlwZSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB2bS5mcnVpdHMucHVzaChyZXNwb25zZS5kYXRhLml0ZW0pO1xyXG4gICAgICAgICAgICBmZXRjaERhdGEodm0uZnJ1aXRzKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZXJyb3IpIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIGZldGNoRGF0YShhcnJheSwgdHlwZSl7XHJcbiAgICAgIHZhciBpbml0aWFsVmFsdWUgPSB7fTtcclxuXHJcbiAgICAgIGlmKHR5cGUgPT0gXCJ2ZWdldGFibGVcIil7XHJcbiAgICAgICAgdm0udmVnZXRhYmxlc09iaiA9IGFycmF5LnJlZHVjZShyZWR1Y2VyLCBpbml0aWFsVmFsdWUpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHZtLmZydWl0T2JqID0gYXJyYXkucmVkdWNlKHJlZHVjZXIsIGluaXRpYWxWYWx1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGZ1bmN0aW9uIHJlZHVjZXIoZnJ1aXQsIGNvdW50KXtcclxuICAgICAgICAgIGlmKCFmcnVpdFtjb3VudF0pe1xyXG4gICAgICAgICAgICAgIGZydWl0W2NvdW50XSA9IDE7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIGZydWl0W2NvdW50XSA9IGZydWl0W2NvdW50XSArIDE7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICByZXR1cm4gZnJ1aXQ7XHJcbiAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gY2xlYXJGZXRjaCgpIHtcclxuICAgICAgdm0udmVnZXRhYmxlc09iaiA9IHZtLmZydWl0T2JqID0ge307XHJcbiAgICAgIHZtLnZlZ2V0YWJsZXMgPSB2bS5mcnVpdHMgPSBbXTtcclxuICAgIH1cclxuICB9O1xyXG59KSgpO1xyXG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
