var gulp    = require('gulp');
var	connect = require('gulp-connect');
var concat  = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();

/*------------------------------------server---------------------------------------*/

gulp.task('connect', function () {

    browserSync.init({
      server: '.'
    });

    browserSync.watch('public/**/*.*').on('change', browserSync.reload);
});

/*------------------------------------dev js only---------------------------------------*/

gulp.task('js-dev', function () {
	return gulp.src(['app/**/*.module.js', 'app/**/*.js'])
        .pipe(sourcemaps.init())
      	.pipe(concat('main.js'))
        .pipe(sourcemaps.write())
      	.pipe(gulp.dest('public/js'));
});

gulp.task('uncompressed', gulp.parallel('connect', 'js-dev'));

gulp.task('watch', function(){
  gulp.watch('app/**/*.js', gulp.series('js-dev'));
})


gulp.task('dev', gulp.parallel('uncompressed', 'watch'));

gulp.task('default', function() {
  console.log('no actions. use \'dev\' or \'prod\' tasks ')
});